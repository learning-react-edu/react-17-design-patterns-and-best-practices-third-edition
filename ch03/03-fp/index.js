// First-class functions & Purity
const add = (x, y) => x + y;

const log = (fn) => (...args) => {
    console.log(`Executing function ${fn} with arguments ${args}`);
    return fn(...args);
};

const logAdd = log(add);

console.log(logAdd(2, 3));

// Immutability
const add3 = arr => arr.push(3)
const myArr = [1, 2]
console.log('before:', myArr);

add3(myArr); // [1, 2, 3]
console.log('after 1st:', myArr);
add3(myArr); // [1, 2, 3, 3]
console.log('after 2nd:', myArr);

const add3Immutable = arr => arr.concat(3)
const myArrImm = [1, 2]
console.log('before:', myArrImm);
const result1 = add3Immutable(myArrImm) // [1, 2, 3]
console.log('result1:', result1);
const result2 = add3Immutable(myArrImm) // [1, 2, 3]
console.log('result2:', result2);
console.log('after:', myArrImm);

// Currying
const addCurried = x => y => x + y;
const add1 = addCurried(1);
console.log(add1(2));
console.log(add1(3));

// Composition
const square = x => x * x;

const addAndSquare = (x, y) => square(add(x, y));
console.log(addAndSquare(2, 3));
