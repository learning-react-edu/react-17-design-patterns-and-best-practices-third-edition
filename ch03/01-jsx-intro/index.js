const createLink = () => {
    console.log('About to create link...');
}

<>
    <div>
    <a href="https://www.js.education">Click me!</a>
    </div>

    const variable = "Hello";

    <div>
    Hello, {variable}. 
    I'm a {() => console.log("Function")}.
    </div>

    <a href={this.createLink()}>Click me!</a>

    <label className="awesome-label" htmlFor="name" />

    <div style={{ backgroundColor: 'red' }} />
    
    <div style={styles} />

    <div> 
    <span>My</span> 
    {' '}
    name is 
    {' '}
    <span>Edu</span> 
    </div>

    <button disabled /> 

    <button disabled={false} /> 

    <button 
        foo="bar" 
        veryLongPropertyName="baz" 
        onSomething={this.handleSomething} 
    />
</>

const styles = {
    backgroundColor: 'red'
};

const getElements = () => {
    return [
        <li key="1">First item</li>, 
        <li key="2">Second item</li>, 
        <li key="3">Third item</li>
        ];
};

const attrs = { 
    id: 'myId',
    className: 'myClass',
};

const elementWithAttributes = () => {
    return <div {...attrs} />;
};

const name = `Edu`;
const multilineHtml = `<p>
This is a multiline string
</p>`;
console.log(`Hi, my name is ${name}`);

// conditionals
const renderButton = () => {
    let button;
    if (isLoggedIn) { 
        button = <LogoutButton />
    } 
    return <div>{button}</div>;
}

<div> 
    {isLoggedIn && <LoginButton />} 
</div>

const renderLogoutOrLogin = () => {
    let button;
    if (isLoggedIn) { 
        button = <LogoutButton />
    } else { 
        button = <LoginButton />;
    }
        
    return <div>{button}</div>;
}
<>
    <div> 
        {isLoggedIn ? <LogoutButton /> : <LoginButton />} 
    </div>

    <div>
        {dataIsReady && (isAdmin || userHasPermissions) && 
            <SecretData />
        }
    </div>
</>

const canShowSecretData = () => { 
    const { dataIsReady, isAdmin, userHasPermissions } = props;
    return dataIsReady && (isAdmin || userHasPermissions);
} 

<div> 
    {this.canShowSecretData() && <SecretData />} 
</div>

const getPrice = () => { 
    return `${props.currency}${props.value}`
}
    
<div>{getPrice()}</div>

// renderIf
import { renderIf } from 'render-if';
const { dataIsReady, isAdmin, userHasPermissions } = props;

const canShowSecretData2 = renderIf( 
    dataIsReady && (isAdmin || userHasPermissions) 
);

<div> 
    {canShowSecretData2(<SecretData />)} 
</div> 

// onlyIf
import onlyIf from 'react-only-if';

const SecretDataOnlyIf = onlyIf(
    ({ dataIsReady, isAdmin, userHasPermissions }) => dataIsReady && 
    (isAdmin || userHasPermissions)
)(SecretData)
    
const MyComponent = () => (
    <div>
        <SecretDataOnlyIf 
            dataIsReady={isReady}
            isAdmin={isAdmin}
            userHasPermissions={hasPermissions}
        />
    </div>
);

// Loops
<ul> 
    {users.map(user => <li>{user.name}</li>)} 
</ul>
constole.log('end of loop');

// Control statements
<If condition={this.canShowSecretData}> 
    <SecretData /> 
</If>
constole.log('If ends');

<Choose> 
    <When condition={firstOption}> 
        <span>if</span> 
    </When> 
    <When condition={secondOption}> 
        <span>else if</span> 
    </When> 
    <Otherwise> 
        <span>else</span> 
    </Otherwise> 
</Choose>
console.log('end of Choose');

<ul> 
    <For each="user" of={this.props.users}> 
        <li>{user.name}</li> 
    </For> 
</ul>
console.log('end of For');

// sub-rendering
const renderUserMenu = () => { 
    // JSX for user menu 
} 
    
const renderAdminMenu = () => { 
    // JSX for admin menu 
} 
    
<div> 
    <h1>Welcome back!</h1> 
    {userExists && renderUserMenu()} 
    {userIsAdmin && renderAdminMenu()} 
</div> 
