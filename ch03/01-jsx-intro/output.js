"use strict";

var _renderIf = require("render-if");

var _reactOnlyIf = _interopRequireDefault(require("react-only-if"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var createLink = function createLink() {
  console.log('About to create link...');
};

/*#__PURE__*/
React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("a", {
  href: "https://www.js.education"
}, "Click me!")), "const variable = \"Hello\";", /*#__PURE__*/React.createElement("div", null, "Hello, ", variable, ". I'm a ", function () {
  return console.log("Function");
}, "."), /*#__PURE__*/React.createElement("a", {
  href: (void 0).createLink()
}, "Click me!"), /*#__PURE__*/React.createElement("label", {
  className: "awesome-label",
  htmlFor: "name"
}), /*#__PURE__*/React.createElement("div", {
  style: {
    backgroundColor: 'red'
  }
}), /*#__PURE__*/React.createElement("div", {
  style: styles
}), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("span", null, "My"), ' ', "name is", ' ', /*#__PURE__*/React.createElement("span", null, "Edu")), /*#__PURE__*/React.createElement("button", {
  disabled: true
}), /*#__PURE__*/React.createElement("button", {
  disabled: false
}), /*#__PURE__*/React.createElement("button", {
  foo: "bar",
  veryLongPropertyName: "baz",
  onSomething: (void 0).handleSomething
}));
var styles = {
  backgroundColor: 'red'
};

var getElements = function getElements() {
  return [/*#__PURE__*/React.createElement("li", {
    key: "1"
  }, "First item"), /*#__PURE__*/React.createElement("li", {
    key: "2"
  }, "Second item"), /*#__PURE__*/React.createElement("li", {
    key: "3"
  }, "Third item")];
};

var attrs = {
  id: 'myId',
  className: 'myClass'
};

var elementWithAttributes = function elementWithAttributes() {
  return /*#__PURE__*/React.createElement("div", attrs);
};

var name = "Edu";
var multilineHtml = "<p>\nThis is a multiline string\n</p>";
console.log("Hi, my name is ".concat(name)); // conditionals

var renderButton = function renderButton() {
  var button;

  if (isLoggedIn) {
    button = /*#__PURE__*/React.createElement(LogoutButton, null);
  }

  return /*#__PURE__*/React.createElement("div", null, button);
};

/*#__PURE__*/
React.createElement("div", null, isLoggedIn && /*#__PURE__*/React.createElement(LoginButton, null));

var renderLogoutOrLogin = function renderLogoutOrLogin() {
  var button;

  if (isLoggedIn) {
    button = /*#__PURE__*/React.createElement(LogoutButton, null);
  } else {
    button = /*#__PURE__*/React.createElement(LoginButton, null);
  }

  return /*#__PURE__*/React.createElement("div", null, button);
};

/*#__PURE__*/
React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", null, isLoggedIn ? /*#__PURE__*/React.createElement(LogoutButton, null) : /*#__PURE__*/React.createElement(LoginButton, null)), /*#__PURE__*/React.createElement("div", null, dataIsReady && (isAdmin || userHasPermissions) && /*#__PURE__*/React.createElement(SecretData, null)));

var canShowSecretData = function canShowSecretData() {
  var _props = props,
      dataIsReady = _props.dataIsReady,
      isAdmin = _props.isAdmin,
      userHasPermissions = _props.userHasPermissions;
  return dataIsReady && (isAdmin || userHasPermissions);
};

/*#__PURE__*/
React.createElement("div", null, (void 0).canShowSecretData() && /*#__PURE__*/React.createElement(SecretData, null));

var getPrice = function getPrice() {
  return "".concat(props.currency).concat(props.value);
};

/*#__PURE__*/
React.createElement("div", null, getPrice()); // renderIf

var _props2 = props,
    dataIsReady = _props2.dataIsReady,
    isAdmin = _props2.isAdmin,
    userHasPermissions = _props2.userHasPermissions;
var canShowSecretData2 = (0, _renderIf.renderIf)(dataIsReady && (isAdmin || userHasPermissions));

/*#__PURE__*/
React.createElement("div", null, canShowSecretData2( /*#__PURE__*/React.createElement(SecretData, null))); // onlyIf

var SecretDataOnlyIf = (0, _reactOnlyIf["default"])(function (_ref) {
  var dataIsReady = _ref.dataIsReady,
      isAdmin = _ref.isAdmin,
      userHasPermissions = _ref.userHasPermissions;
  return dataIsReady && (isAdmin || userHasPermissions);
})(SecretData);

var MyComponent = function MyComponent() {
  return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(SecretDataOnlyIf, {
    dataIsReady: isReady,
    isAdmin: isAdmin,
    userHasPermissions: hasPermissions
  }));
}; // Loops


/*#__PURE__*/
React.createElement("ul", null, users.map(function (user) {
  return /*#__PURE__*/React.createElement("li", null, user.name);
}));
constole.log('end of loop'); // Control statements

(void 0).canShowSecretData ? /*#__PURE__*/React.createElement(SecretData, null) : null;
constole.log('If ends');
firstOption ? /*#__PURE__*/React.createElement("span", null, "if") : secondOption ? /*#__PURE__*/React.createElement("span", null, "else if") : /*#__PURE__*/React.createElement("span", null, "else");
